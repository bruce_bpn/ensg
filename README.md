# Support rédigés pour l'ENSG

Bonjour à tous ! Je dépose sur ce Git tous les documents que j'ai pu rédiger pour l'ENSG dans le cadre de mes trois ans d'étude.

## Supports déposés
### Topométrie

Création d'un fichier pour calculer le gisement, le G0 pondéré, le G0 moyen et le rayonnement d'un point.
Possibilité d'enchaîner le calcul d'un G0 pondéré en saissant les coordonnées des points visés à partir d'une station.
